import 'package:engshow/pages/page_home.dart';
import 'package:engshow/pages/page_home_root.dart';
import 'package:engshow/pages/page_splash.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Eng Show',
      theme: ThemeData(
          primarySwatch: Colors.red,
          primaryColor: Color(0xffffffff),
          backgroundColor: Color(0xFF171010),
          fontFamily: 'Open Sans',
          textTheme: TextTheme(
              bodyText1: TextStyle(fontFamily: 'Open Sans', letterSpacing: 0.2),
              bodyText2:
                  TextStyle(fontFamily: 'Open Sans', letterSpacing: 0.2))),
      home: SplashScreen(),
    );
  }
}
