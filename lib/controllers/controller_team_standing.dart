import 'dart:convert';

import 'package:engshow/models/model_team_standing.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class ControllerTeamStanding extends GetxController {
  RxList<ModelTeamStanding> teamStanding = <ModelTeamStanding>[].obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    fetchStanding();
  }

  Future<void> fetchStanding() async {
    var url = Uri.parse(
        'https://api.football-data.org/v2/competitions/2021/standings?standingType=HOME');
    var response = await http.get(url,
        headers: {'X-Auth-Token': 'de3e847e4a234084873e5310f0f10d1e'});
    var standing = jsonDecode(response.body);
    await setStanding(standing['standings'][0]['table']);
  }

  setStanding(List _standing) {
    List<ModelTeamStanding> listStand = <ModelTeamStanding>[];
    for (var stand in _standing) {
      listStand.add(ModelTeamStanding(
          teamId: stand['team']['id'].toString(),
          position: stand['position'],
          teamCrest: stand['team']['crestUrl'],
          teamName: stand['team']['name'],
          playedGames: stand['playedGames'],
          goalDifference: stand['goalDifference'],
          points: stand['points'],
          draw: stand['draw'],
          goalsAgainst: stand['goalsAgainst'],
          goalsFor: stand['goalsFor'],
          lost: stand['lost'],
          won: stand['won']));
    }
    teamStanding.value = listStand;
  }
}
