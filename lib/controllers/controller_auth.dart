import 'package:engshow/pages/page_home.dart';
import 'package:engshow/pages/page_home_root.dart';
import 'package:engshow/pages/page_login.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

class FirebaseController extends GetxController {
  final Rxn<User> _firebaseUser = Rxn<User>();

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  String? get user => _firebaseUser.value!.email;
  @override
  void onInit() async {
    // await Firebase.initializeApp();
    _firebaseUser.bindStream(_firebaseAuth.authStateChanges());
  }

  // function to createuser, login and sign out user
  void createUser(String _email, String _password) async {
    try {
      await _firebaseAuth
          .createUserWithEmailAndPassword(email: _email, password: _password)
          .then((value) {
        Get.offAll(LoginPage());
        Get.snackbar(
            "Registration Successful", 'Please log in with your account',
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.green,
            colorText: Colors.white,
            margin: EdgeInsets.all(16));
      }).catchError(
        (onError) => Get.snackbar(
            "Error while creating account ", onError.message,
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(16)),
      );
    } catch (e) {
      Get.snackbar("Error while creating account ", e.toString(),
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: Colors.red,
          colorText: Colors.white,
          margin: EdgeInsets.all(16));
    }
  }

  void login(String email, String password) async {
    try {
      await _firebaseAuth
          .signInWithEmailAndPassword(email: email, password: password)
          .then((value) => Get.offAll(HomeRoot()))
          .catchError((onError) => Get.snackbar(
              "Error while sign in ", onError.message,
              snackPosition: SnackPosition.BOTTOM,
              backgroundColor: Colors.red,
              colorText: Colors.white,
              margin: EdgeInsets.all(16)));
    } on Exception catch (e) {
      Get.snackbar("Error while sign in ", e.toString(),
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: Colors.red,
          colorText: Colors.white,
          margin: EdgeInsets.all(16));
    }
  }

  void signout() async {
    await _firebaseAuth.signOut().then((value) => Get.offAll(LoginPage()));
  }
}
