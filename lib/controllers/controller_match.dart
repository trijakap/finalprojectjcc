import 'dart:convert';

import 'package:engshow/models/model_match.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class ControllerMatch extends GetxController {
  final RxList<ModelMatch> matches = <ModelMatch>[].obs;

  @override
  void onInit() {
    super.onInit();
    fetchMatches();
  }

  Future<void> fetchMatches() async {
    try {
      var url = Uri.parse(
          'https://api.football-data.org/v2/competitions/2021/matches?season=2021&id=66');
      var response = await http.get(url,
          headers: {'X-Auth-Token': 'de3e847e4a234084873e5310f0f10d1e'});
      var match = jsonDecode(response.body);

      url = Uri.parse('https://api.football-data.org/v2/teams');
      response = await http.get(url,
          headers: {'X-Auth-Token': 'de3e847e4a234084873e5310f0f10d1e'});
      var team = jsonDecode(response.body);

      await setMatches(match['matches'], team['teams']);
    } catch (e) {}
  }

  setMatches(List _matches, List _teams) {
    List<ModelMatch> listMatches = <ModelMatch>[];

    for (var match in _matches) {
      if (match['season']['currentMatchday'] == match['matchday']) {
        var homeTeam, awayTeam;

        for (var team in _teams) {
          team['id'] == match['homeTeam']['id']
              ? homeTeam = team
              : (team['id'] == match['awayTeam']['id']
                  ? awayTeam = team
                  : null);
        }

        listMatches.add(ModelMatch(
            matchday: match['season']['currentMatchday'],
            homeTeamName: homeTeam['shortName'],
            awayTeamName: awayTeam['shortName'],
            venue: homeTeam['venue'],
            date: DateTime.parse(match['utcDate']),
            homeTeamCrest: homeTeam['crestUrl'],
            awayTeamCrest: awayTeam['crestUrl'],
            gameStatus: match['status'],
            homeTeamScore: match['score']['fullTime']['homeTeam'],
            awayTeamScore: match['score']['fullTime']['awayTeam']));
      }
      ;
    }
    matches.value = listMatches;
  }
}
