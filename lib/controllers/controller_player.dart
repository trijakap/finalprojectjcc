import 'dart:async';
import 'dart:convert';

import 'package:engshow/controllers/polish_word_normalizer.dart';
import 'package:engshow/models/model_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class ControllerPlayer extends GetxController {
  RxList<ModelPlayer> player = <ModelPlayer>[].obs;

  late String _teamIdx;
  // final VoidCallback snackCallBack;

  // ControllerPlayer(this.snackCallBack);
  @override
  void onInit() {
    super.onInit();
  }

  updateTeamId(String teamIdx) {
    _teamIdx = teamIdx;
    update();
  }

  Future<void> fetchPlayer() async {
    try {
      var url = Uri.parse('https://api.football-data.org/v2/teams/$_teamIdx');
      var response = await http.get(url,
          headers: {'X-Auth-Token': 'de3e847e4a234084873e5310f0f10d1e'});
      var players = jsonDecode(response.body);

      try {
        url = Uri.parse(
            'https://fantasy.premierleague.com/api/bootstrap-static/');
        response =
            await http.get(url).timeout(Duration(seconds: 7), onTimeout: () {
          // Time has run out, do what you wanted to do.
          return http.Response(
              'Error', 500); // Replace 500 with your http code.
        });
      } on Exception catch (e) {
        Get.snackbar('Error', e.toString(),
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            margin: EdgeInsets.all(16));
      }

      var fpl = jsonDecode(response.body);
      await setPlayer(players['squad'], fpl['elements']);
    } on Exception catch (e) {
      Get.snackbar('Error', e.toString(),
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: Colors.red,
          colorText: Colors.white,
          margin: EdgeInsets.all(16));
    }
  }

  retryFuture(future, delay) {
    Future.delayed(Duration(milliseconds: delay), () {
      future();
    });
  }

  setPlayer(List _players, _fpl) {
    List<ModelPlayer> listPlayer = <ModelPlayer>[];
    for (var player in _players) {
      String photoIdx = '';
      for (var fpl in _fpl) {
        String fullName = player['name'];
        String fullNameNormalized = '';
        for (int i = 0; i < fullName.length; i++) {
          fullNameNormalized += normalizeChar(fullName[i]);
        }

        String webName = fpl['web_name'];
        String webNameNormalized = '';
        for (int i = 0; i < webName.length; i++) {
          webNameNormalized += normalizeChar(webName[i]);
        }

        if (fullNameNormalized.contains(webNameNormalized)) {
          photoIdx = fpl['code'].toString();
        }
      }
      listPlayer.add(ModelPlayer(
          playerId: player['id'].toString(),
          playerName: player['name'],
          position: player['position'] ?? '',
          nationality: player['nationality'] ?? '',
          photoId: photoIdx));
    }
    player.value = listPlayer;
    player.sort((a, b) => a.position.compareTo(b.position));
  }
}
