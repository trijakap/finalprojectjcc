import 'package:engshow/controllers/controller_match.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  final controllerMatch = Get.put(ControllerMatch());

  var _matchDay, _matchMonth, _matchYear, _matchDate;
  late bool _newDay;

  List weekday = [
    '',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday'
  ];

  List month = [
    '',
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: ListView(
        shrinkWrap: true,
        children: [
          Center(
            child: Container(
              margin: EdgeInsets.fromLTRB(0, 48, 0, 40),
              width: 344,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.menu,
                      size: 24,
                      color: Colors.white,
                    ),
                  ),
                  Image(
                    image: AssetImage('lib/shared/engshow_logo.png'),
                    width: 80,
                    fit: BoxFit.contain,
                  ),
                  CircleAvatar(
                    backgroundColor: Color(0xFFC4C4C4),
                    radius: 24,
                    child: Icon(
                      Icons.person,
                      size: 24,
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ),
          ),
          Container(child: Obx(() {
            if (controllerMatch.matches.isNotEmpty) {
              return Column(children: [
                if (controllerMatch.matches.firstWhere(
                        (element) => element.gameStatus == 'FINISHED') !=
                    null)
                  Container(
                    width: 344,
                    padding: EdgeInsets.fromLTRB(0, 24, 0, 48),
                    child: Column(
                      children: [
                        Text(
                          'Match Result',
                          style: TextStyle(
                              fontSize: 20,
                              letterSpacing: 0.3,
                              fontWeight: FontWeight.w700,
                              color: const Color(0xFFFFB830)),
                        ),
                        ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: controllerMatch.matches.length,
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemBuilder: (context, index) {
                              if (controllerMatch.matches[index].gameStatus ==
                                  'FINISHED') {
                                return Container(
                                  margin: EdgeInsets.only(top: 12, bottom: 12),
                                  padding: EdgeInsets.all(24),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: Color(0xFF2B2B2B),
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Center(
                                          child: SvgPicture.network(
                                              controllerMatch
                                                  .matches[index].homeTeamCrest,
                                              height: 64,
                                              width: 64,
                                              fit: BoxFit.contain)),
                                      Center(
                                        child: Column(
                                          children: [
                                            Container(
                                              width: 120,
                                              child: Text(
                                                '${controllerMatch.matches[index].homeTeamName} - ${controllerMatch.matches[index].awayTeamName}',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w700,
                                                    color: const Color(
                                                        0xFFFFB830)),
                                              ),
                                            ),
                                            SizedBox(height: 12),
                                            Container(
                                              width: 120,
                                              child: Text(
                                                '${controllerMatch.matches[index].homeTeamScore} - ${controllerMatch.matches[index].awayTeamScore}',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 32,
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w700,
                                                    color: Colors.white),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Center(
                                          child: SvgPicture.network(
                                              controllerMatch
                                                  .matches[index].awayTeamCrest,
                                              height: 64,
                                              width: 64,
                                              fit: BoxFit.contain))
                                    ],
                                  ),
                                );
                              } else
                                return Container();
                            })
                      ],
                    ),
                  ),
                Container(
                  color: Color(0xFF2B2B2B),
                  padding: EdgeInsets.fromLTRB(16, 40, 16, 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Matchday ${controllerMatch.matches[0].matchday}',
                        style: TextStyle(
                            fontFamily: 'Cambria',
                            fontSize: 48,
                            letterSpacing: 0.3,
                            fontWeight: FontWeight.w700,
                            color: Theme.of(context).primaryColor),
                      ),
                      ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: controllerMatch.matches.length,
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemBuilder: (context, index) {
                          _newDay = false;
                          if (index == 0 ||
                              (controllerMatch.matches[index].date.day !=
                                      _matchDate ||
                                  controllerMatch.matches[index].date.month !=
                                      _matchMonth ||
                                  controllerMatch.matches[index].date.year !=
                                      _matchYear)) {
                            _matchDay = weekday[
                                controllerMatch.matches[index].date.weekday];
                            _matchDate =
                                controllerMatch.matches[index].date.day;
                            _matchMonth =
                                controllerMatch.matches[index].date.month;
                            _matchYear =
                                controllerMatch.matches[index].date.year;
                            _newDay = true;
                          }
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              if (_newDay)
                                SizedBox(
                                  height: 40,
                                ),
                              if (_newDay)
                                Text(
                                    '$_matchDay, $_matchDate ${month[_matchMonth]} $_matchYear',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xFFFFB830),
                                        letterSpacing: 0.3,
                                        fontSize: 12)),
                              SizedBox(
                                height: 32,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      CircleAvatar(
                                          backgroundColor: Color(0xFF423F3E),
                                          radius: 17,
                                          child: Container(
                                            height: 24,
                                            width: 24,
                                            child: SvgPicture.network(
                                                controllerMatch.matches[index]
                                                    .homeTeamCrest),
                                          )),
                                      SizedBox(
                                        width: 4,
                                      ),
                                      CircleAvatar(
                                          backgroundColor: Color(0xFF423F3E),
                                          radius: 17,
                                          child: Container(
                                            height: 24,
                                            width: 24,
                                            child: SvgPicture.network(
                                                controllerMatch.matches[index]
                                                    .awayTeamCrest),
                                          )),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: 174,
                                            child: Text(
                                                '${controllerMatch.matches[index].homeTeamName} VS ${controllerMatch.matches[index].awayTeamName}',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w700,
                                                    color: Color(0xFFFFFFFF),
                                                    letterSpacing: 0.3,
                                                    fontSize: 16)),
                                          ),
                                          Text(
                                              controllerMatch
                                                  .matches[index].venue,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  color: Color(0x50FFFFFF),
                                                  letterSpacing: 0.3,
                                                  fontSize: 12))
                                        ],
                                      ),
                                    ],
                                  ),
                                  Container(
                                      width: 40,
                                      child: Text(
                                        controllerMatch.matches[index].date.hour
                                                .toString() +
                                            ':' +
                                            (controllerMatch.matches[index].date
                                                        .minute ==
                                                    0
                                                ? '00'
                                                : controllerMatch
                                                    .matches[index].date.minute
                                                    .toString()),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700,
                                            color: Color(0xFFFFFFFF),
                                            letterSpacing: 0.3,
                                            fontSize: 16),
                                      ))
                                ],
                              ),
                            ],
                          );
                        },
                      ),
                    ],
                  ),
                )
              ]);
            } else {
              return const Center(
                child: Text('Fetching Data from Database',
                    style: TextStyle(
                        fontSize: 16,
                        letterSpacing: 0.3,
                        fontWeight: FontWeight.w600,
                        color: Colors.white54)),
              );
            }
          }))
        ],
      ),
    );
  }
}
