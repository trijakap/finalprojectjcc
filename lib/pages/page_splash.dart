import 'dart:async';
import 'package:engshow/pages/page_login.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(const Duration(seconds: 3), () => Get.off(LoginPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF171010),
        body: Center(
          child: Column(children: [
            const SizedBox(
              height: 160,
              child: Center(
                child: Image(
                  image: AssetImage('lib/shared/engshow_logo.png'),
                ),
              ),
            ),
            Expanded(
              child: ShaderMask(
                shaderCallback: (rect) {
                  return LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: [Colors.black, Colors.transparent],
                  ).createShader(Rect.fromLTRB(0, 0, rect.width, rect.height));
                },
                blendMode: BlendMode.dstIn,
                child: Image(
                  image: AssetImage('lib/shared/splash_cover.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            )
          ]),
        ));
  }
}
