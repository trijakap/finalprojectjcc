import 'package:engshow/controllers/controller_bottom_nav.dart';
import 'package:engshow/pages/page_home.dart';
import 'package:engshow/pages/page_profile.dart';
import 'package:engshow/pages/page_standing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeRoot extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bottomNavController = Get.put(BottomNavController());
    return Scaffold(
      bottomNavigationBar:
          buildBottomNavigationMenu(context, bottomNavController),
      body: Obx(() => IndexedStack(
            index: bottomNavController.tabIndex.value,
            children: [
              HomePage(),
              StandingPage(),
              ProfilePage(),
            ],
          )),
    );
  }

  final TextStyle unselectedLabelStyle = TextStyle(
      color: Colors.white.withOpacity(0.5),
      fontWeight: FontWeight.w500,
      fontSize: 12);

  final TextStyle selectedLabelStyle =
      TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 12);

  buildBottomNavigationMenu(context, landingPageController) {
    return Obx(() => MediaQuery(
        data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        child: Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  color: Color(0x50423F3E), spreadRadius: 4, blurRadius: 8),
            ],
          ),
          height: 64,
          child: BottomNavigationBar(
            showUnselectedLabels: false,
            showSelectedLabels: false,
            onTap: landingPageController.changeTabIndex,
            currentIndex: landingPageController.tabIndex.value,
            backgroundColor: Color(0xFF423F3E),
            unselectedItemColor: Colors.white,
            selectedItemColor: Color(0xFFFFB830),
            items: [
              BottomNavigationBarItem(
                icon: Container(
                  child: Icon(
                    Icons.home,
                    size: 24.0,
                  ),
                ),
                label: 'Home',
                backgroundColor: Color.fromRGBO(36, 54, 101, 1.0),
              ),
              BottomNavigationBarItem(
                icon: Container(
                  child: Icon(
                    Icons.format_list_numbered,
                    size: 24.0,
                  ),
                ),
                label: 'Explore',
                backgroundColor: Color.fromRGBO(36, 54, 101, 1.0),
              ),
              BottomNavigationBarItem(
                icon: Container(
                  child: Icon(
                    Icons.person,
                    size: 24.0,
                  ),
                ),
                label: 'Places',
                backgroundColor: Color.fromRGBO(36, 54, 101, 1.0),
              ),
            ],
          ),
        )));
  }
}
