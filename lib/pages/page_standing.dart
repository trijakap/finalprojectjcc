import 'package:engshow/controllers/controller_team_standing.dart';
import 'package:engshow/pages/page_detail_team.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_svg/svg.dart';

class StandingPage extends StatelessWidget {
  final controllerTeamStanding = Get.put(ControllerTeamStanding());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
        child: ListView(
          shrinkWrap: true,
          children: [
            Center(
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 48, 0, 40),
                width: 344,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.menu,
                        size: 24,
                        color: Colors.white,
                      ),
                    ),
                    Image(
                      image: AssetImage('lib/shared/engshow_logo.png'),
                      width: 80,
                      fit: BoxFit.contain,
                    ),
                    CircleAvatar(
                      backgroundColor: Color(0xFFC4C4C4),
                      radius: 24,
                      child: Icon(
                        Icons.person,
                        size: 24,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
                child: Obx(() => controllerTeamStanding.teamStanding.isNotEmpty
                    ? Container(
                        color: Color(0xFF2B2B2B),
                        padding: EdgeInsets.fromLTRB(16, 40, 16, 24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'EPL Standings',
                              style: TextStyle(
                                  fontFamily: 'Cambria',
                                  fontSize: 48,
                                  letterSpacing: 0.3,
                                  fontWeight: FontWeight.w700,
                                  color: Theme.of(context).primaryColor),
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  width: 46,
                                  child: Center(
                                    child: Text('Pos',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            color: Color(0xFFFFB830),
                                            letterSpacing: 0.3,
                                            fontSize: 12)),
                                  ),
                                ),
                                Container(
                                  width: 167,
                                  child: Center(
                                    child: Text('Club',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            color: Color(0xFFFFB830),
                                            letterSpacing: 0.3,
                                            fontSize: 12)),
                                  ),
                                ),
                                Container(
                                  width: 46,
                                  child: Center(
                                    child: Text('P',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            color: Color(0xFFFFB830),
                                            letterSpacing: 0.3,
                                            fontSize: 12)),
                                  ),
                                ),
                                Container(
                                  width: 46,
                                  child: Center(
                                    child: Text('GD',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            color: Color(0xFFFFB830),
                                            letterSpacing: 0.3,
                                            fontSize: 12)),
                                  ),
                                ),
                                Container(
                                    width: 39,
                                    child: Center(
                                      child: Text('Pts',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              color: Color(0xFFFFB830),
                                              letterSpacing: 0.3,
                                              fontSize: 12)),
                                    )),
                              ],
                            ),
                            ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount:
                                    controllerTeamStanding.teamStanding.length,
                                itemBuilder: (context, index) {
                                  return Padding(
                                      padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                                      child: InkWell(
                                        onTap: () {
                                          try {
                                            Get.to(() => DetailTeamPage(),
                                                arguments:
                                                    controllerTeamStanding
                                                        .teamStanding[index]
                                                        .teamId);
                                          } on Exception catch (e) {
                                            Get.snackbar('Error',
                                                'We occured some error, please try again later',
                                                snackPosition:
                                                    SnackPosition.BOTTOM,
                                                backgroundColor: Colors.red,
                                                colorText: Colors.white,
                                                margin: EdgeInsets.all(16));
                                          }
                                        },
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Container(
                                                width: 46,
                                                child: Ink(
                                                  color: Colors.transparent,
                                                  child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        if ([1, 2, 3].contains(
                                                            controllerTeamStanding
                                                                .teamStanding[
                                                                    index]
                                                                .position))
                                                          CircleAvatar(
                                                              backgroundColor:
                                                                  Colors.blue,
                                                              radius: 4),
                                                        if ([4].contains(
                                                            controllerTeamStanding
                                                                .teamStanding[
                                                                    index]
                                                                .position))
                                                          CircleAvatar(
                                                              backgroundColor:
                                                                  Colors.blue[
                                                                      200],
                                                              radius: 4),
                                                        if ([5].contains(
                                                            controllerTeamStanding
                                                                .teamStanding[
                                                                    index]
                                                                .position))
                                                          CircleAvatar(
                                                              backgroundColor:
                                                                  Colors.orange,
                                                              radius: 4),
                                                        if ([
                                                          1,
                                                          2,
                                                          3,
                                                          4,
                                                          5
                                                        ].contains(
                                                            controllerTeamStanding
                                                                .teamStanding[
                                                                    index]
                                                                .position))
                                                          SizedBox(
                                                            width: 4,
                                                          ),
                                                        Text(
                                                            controllerTeamStanding
                                                                .teamStanding[
                                                                    index]
                                                                .position
                                                                .toString(),
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700,
                                                                color: Color(
                                                                    0xFFFFFFFF),
                                                                letterSpacing:
                                                                    0.3,
                                                                fontSize: 16)),
                                                      ]),
                                                )),
                                            Container(
                                                width: 167,
                                                child: Row(
                                                  children: [
                                                    CircleAvatar(
                                                        backgroundColor:
                                                            Color(0xFF423F3E),
                                                        radius: 17,
                                                        child: Container(
                                                          height: 24,
                                                          width: 24,
                                                          child: SvgPicture.network(
                                                              controllerTeamStanding
                                                                  .teamStanding[
                                                                      index]
                                                                  .teamCrest),
                                                        )),
                                                    SizedBox(
                                                      width: 8,
                                                    ),
                                                    Container(
                                                      width: 125,
                                                      child: Text(
                                                          controllerTeamStanding
                                                              .teamStanding[
                                                                  index]
                                                              .teamName,
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                              color: Color(
                                                                  0xFFFFFFFF),
                                                              letterSpacing:
                                                                  0.3,
                                                              fontSize: 16)),
                                                    ),
                                                  ],
                                                )),
                                            Container(
                                              width: 46,
                                              child: Center(
                                                child: Text(
                                                    controllerTeamStanding
                                                        .teamStanding[index]
                                                        .playedGames
                                                        .toString(),
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w300,
                                                        color: Colors.white,
                                                        letterSpacing: 0.3,
                                                        fontSize: 16)),
                                              ),
                                            ),
                                            Container(
                                              width: 46,
                                              child: Center(
                                                child: Text(
                                                    controllerTeamStanding
                                                        .teamStanding[index]
                                                        .goalDifference
                                                        .toString(),
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w300,
                                                        color: Colors.white,
                                                        letterSpacing: 0.3,
                                                        fontSize: 16)),
                                              ),
                                            ),
                                            Container(
                                                width: 39,
                                                child: Center(
                                                  child: Text(
                                                      controllerTeamStanding
                                                          .teamStanding[index]
                                                          .points
                                                          .toString(),
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          color: Colors.white,
                                                          letterSpacing: 0.3,
                                                          fontSize: 16)),
                                                )),
                                          ],
                                        ),
                                      ));
                                })
                          ],
                        ))
                    : const Center(
                        child: Text('Fetching Data from Database',
                            style: TextStyle(
                                fontSize: 16,
                                letterSpacing: 0.3,
                                fontWeight: FontWeight.w600,
                                color: Colors.white54)),
                      )))
          ],
        ),
      ),
    );
  }
}
