import 'package:engshow/controllers/controller_auth.dart';
import 'package:engshow/pages/page_login.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterPage extends StatelessWidget {
  final controllerRegister = Get.put(FirebaseController());
  TextEditingController email = TextEditingController();
  TextEditingController pass = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 160,
                child: Center(
                  child: Image(
                    image: AssetImage('lib/shared/engshow_logo.png'),
                  ),
                ),
              ),
              Container(
                width: 344,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Register",
                      style: TextStyle(
                          fontFamily: 'Cambria',
                          fontSize: 48,
                          letterSpacing: 0.3,
                          fontWeight: FontWeight.w700,
                          color: Theme.of(context).primaryColor),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 16, 0, 8),
                      child: Text(
                        'Email',
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: Color(0x50FFFFFF)),
                      ),
                    ),
                    Container(
                      child: Focus(
                        debugLabel: 'TextFieldRegisterEmail',
                        child: Builder(
                          builder: (BuildContext context) {
                            final FocusNode focusNode = Focus.of(context);
                            final bool hasFocus = focusNode.hasFocus;
                            return TextField(
                              controller: email,
                              decoration: new InputDecoration(
                                  contentPadding:
                                      const EdgeInsets.fromLTRB(16, 23, 16, 20),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Color(0xFFFFB830), width: 1.0),
                                  ),
                                  hintText: 'e.g. BorisJohnson@yahoo.com',
                                  hintStyle: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      color: Color(0x30FFFFFF)),
                                  fillColor: Color(0xFF2B2B2B),
                                  filled: true),
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Theme.of(context).primaryColor,
                                  letterSpacing: 0.2),
                            );
                          },
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 16, 0, 8),
                      child: Text(
                        'Password',
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: Color(0x50FFFFFF)),
                      ),
                    ),
                    Container(
                      child: Focus(
                        debugLabel: 'TextFieldRegisterPassword',
                        child: Builder(
                          builder: (BuildContext context) {
                            final FocusNode focusNode = Focus.of(context);
                            final bool hasFocus = focusNode.hasFocus;
                            return TextField(
                              controller: pass,
                              obscureText: true,
                              enableSuggestions: false,
                              autocorrect: false,
                              decoration: new InputDecoration(
                                  contentPadding:
                                      const EdgeInsets.fromLTRB(16, 23, 16, 20),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Color(0xFFFFB830), width: 1.0),
                                  ),
                                  hintStyle: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      color: Color(0x30FFFFFF)),
                                  fillColor: Color(0xFF2B2B2B),
                                  filled: true),
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Theme.of(context).primaryColor,
                                  letterSpacing: 0.2),
                            );
                          },
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 64),
                      child: ElevatedButton(
                        onPressed: () {
                          if (email.text.isNotEmpty && pass.text.isNotEmpty) {
                            _registerUser(
                                email.text.replaceAll(' ', ''), pass.text);
                          } else {
                            getSnackBar(
                                'Error', 'Email/Password field is empty');
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            fixedSize: const Size(344, 64),
                            shadowColor: const Color(0x75FFB830),
                            primary: const Color(0xFFFFB830),
                            elevation: 7,
                            splashFactory: NoSplash.splashFactory),
                        child: Text(
                          'REGISTER',
                          style: TextStyle(
                              fontSize: 20,
                              letterSpacing: 1,
                              fontWeight: FontWeight.w700,
                              color: const Color(0xFF11052C)),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 32,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Already have the account?",
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Color(0xFFFFFFFF),
                                fontSize: 12)),
                        SizedBox(
                          width: 4,
                        ),
                        InkWell(
                          onTap: () {
                            Get.off(LoginPage());
                          },
                          child: Text("Log in here?",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xFFFFB830),
                                  fontSize: 12)),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 32,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  void _registerUser(String _email, String _pass) {
    controllerRegister.createUser(_email, _pass);
  }

  void getSnackBar(String errType, String errMsg) {
    Get.snackbar(errType, errMsg,
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.red,
        colorText: Colors.white,
        margin: EdgeInsets.all(16));
  }
}
