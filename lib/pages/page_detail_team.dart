import 'package:engshow/controllers/controller_player.dart';
import 'package:engshow/controllers/controller_team_standing.dart';
import 'package:engshow/models/model_team_standing.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DetailTeamPage extends StatelessWidget {
  final dataTeamId = Get.arguments;
  final controllerPlayer = Get.put(ControllerPlayer());
  final controllerTeamStanding = Get.put(ControllerTeamStanding());

  @override
  Widget build(BuildContext context) {
    //   var controllerPlayer = Get.put(ControllerPlayer());
    controllerPlayer.updateTeamId(Get.arguments);
    try {
      controllerPlayer.fetchPlayer();
    } catch (e) {
      Get.snackbar('Error', 'We occured some error, please try again later',
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: Colors.red,
          colorText: Colors.white,
          margin: EdgeInsets.all(16));
    }

    var currentPos = '';
    var newPos;

    ModelTeamStanding teamData =
        getTeamData(controllerTeamStanding, dataTeamId);

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
        child: ListView(
          shrinkWrap: true,
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(16, 64, 16, 28),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextButton.icon(
                    onPressed: () {
                      Get.back();
                    },
                    icon: Icon(Icons.arrow_back_ios, color: Color(0xFFFFB830)),
                    label: Text(
                      'Kembali ke halaman masuk',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          color: Colors.white),
                    ),
                    style: TextButton.styleFrom(
                      primary: Color(0xFF11052C),
                      backgroundColor: Colors.transparent,
                      splashFactory: NoSplash.splashFactory,
                    ),
                  ),
                  SizedBox(
                    height: 64,
                  ),
                  Center(
                    child: CircleAvatar(
                        backgroundColor: Color(0xFF423F3E),
                        radius: 82,
                        child: Container(
                          height: 116,
                          width: 116,
                          child: SvgPicture.network(teamData.teamCrest),
                        )),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Center(
                    child: Text(
                      teamData.teamName,
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w700,
                          color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text('Pos',
                          style: TextStyle(
                              fontSize: 16,
                              letterSpacing: 0.3,
                              fontWeight: FontWeight.w700,
                              color: Color(0xFFFFB830))),
                      SizedBox(
                        width: 8,
                      ),
                      Text(teamData.position.toString(),
                          style: TextStyle(
                              fontSize: 64,
                              letterSpacing: 0.3,
                              fontWeight: FontWeight.w600,
                              color: Colors.white)),
                    ],
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 40,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('W',
                                style: TextStyle(
                                    fontSize: 16,
                                    letterSpacing: 0.3,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFFFFB830))),
                            SizedBox(
                              height: 8,
                            ),
                            Text(teamData.won.toString(),
                                style: TextStyle(
                                    fontSize: 16,
                                    letterSpacing: 0.3,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white)),
                          ],
                        ),
                      ),
                      SizedBox(width: 10),
                      Container(
                        width: 40,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'D',
                              style: TextStyle(
                                  fontSize: 16,
                                  letterSpacing: 0.3,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xFFFFB830)),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              teamData.draw.toString(),
                              style: TextStyle(
                                  fontSize: 16,
                                  letterSpacing: 0.3,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 10),
                      Container(
                          width: 40,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'L',
                                style: TextStyle(
                                    fontSize: 16,
                                    letterSpacing: 0.3,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFFFFB830)),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(teamData.lost.toString(),
                                  style: TextStyle(
                                      fontSize: 16,
                                      letterSpacing: 0.3,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white)),
                            ],
                          ))
                    ],
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 40,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('GA',
                                style: TextStyle(
                                    fontSize: 16,
                                    letterSpacing: 0.3,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFFFFB830))),
                            SizedBox(
                              height: 8,
                            ),
                            Text(teamData.goalsAgainst.toString(),
                                style: TextStyle(
                                    fontSize: 16,
                                    letterSpacing: 0.3,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white)),
                          ],
                        ),
                      ),
                      SizedBox(width: 10),
                      Container(
                        width: 40,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'GF',
                              style: TextStyle(
                                  fontSize: 16,
                                  letterSpacing: 0.3,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xFFFFB830)),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              teamData.goalsFor.toString(),
                              style: TextStyle(
                                  fontSize: 16,
                                  letterSpacing: 0.3,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 10),
                      Container(
                          width: 40,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'GD',
                                style: TextStyle(
                                    fontSize: 16,
                                    letterSpacing: 0.3,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFFFFB830)),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(teamData.goalDifference.toString(),
                                  style: TextStyle(
                                      fontSize: 16,
                                      letterSpacing: 0.3,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white)),
                            ],
                          )),
                      SizedBox(width: 10),
                      Container(
                          width: 40,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'Pts',
                                style: TextStyle(
                                    fontSize: 16,
                                    letterSpacing: 0.3,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFFFFB830)),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(teamData.points.toString(),
                                  style: TextStyle(
                                      fontSize: 16,
                                      letterSpacing: 0.3,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white)),
                            ],
                          ))
                    ],
                  ),
                  SizedBox(
                    height: 40,
                  ),
                ],
              ),
            ),
            Container(child: Obx(() {
              if (controllerPlayer.player.isNotEmpty) {
                return Container(
                    color: Color(0xFF2B2B2B),
                    padding: EdgeInsets.fromLTRB(16, 24, 16, 24),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Squad',
                          style: TextStyle(
                              fontFamily: 'Cambria',
                              fontSize: 32,
                              letterSpacing: 0.3,
                              fontWeight: FontWeight.w700,
                              color: Theme.of(context).primaryColor),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Container(
                          color: Colors.white,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: controllerPlayer.player.length,
                            itemBuilder: (context, index) {
                              newPos = false;
                              if (index == 0 ||
                                  controllerPlayer.player[index].position !=
                                      currentPos) {
                                currentPos =
                                    controllerPlayer.player[index].position;
                                newPos = true;
                              }
                              return Padding(
                                  padding: EdgeInsets.fromLTRB(0, 16, 0, 16),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      if (controllerPlayer.player[index]
                                              .position.isNotEmpty &&
                                          newPos)
                                        Center(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Text(
                                                currentPos,
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w600,
                                                    color: Color(0xFFFFB830)),
                                              ),
                                              SizedBox(
                                                height: 8,
                                              )
                                            ],
                                          ),
                                        ),
                                      Row(
                                        children: [
                                          if (controllerPlayer
                                              .player[index].photoId.isNotEmpty)
                                            Image.network(
                                                'https://resources.premierleague.com/premierleague/photos/players/110x140/p${controllerPlayer.player[index].photoId}.png',
                                                width: 96,
                                                height: 96,
                                                fit: BoxFit.contain),
                                          if (controllerPlayer
                                              .player[index].photoId.isEmpty)
                                            Image.asset(
                                                'lib/shared/silhouette.png',
                                                width: 96,
                                                height: 96,
                                                fit: BoxFit.contain),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                controllerPlayer
                                                    .player[index].playerName,
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w700,
                                                    color: Theme.of(context)
                                                        .primaryColor),
                                              ),
                                              SizedBox(height: 4),
                                              Text(
                                                'Nationality: ${controllerPlayer.player[index].nationality}',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    letterSpacing: 0.3,
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.white54),
                                              ),
                                            ],
                                          )
                                        ],
                                      )
                                    ],
                                  ));
                            })
                      ],
                    ));
              } else {
                return Center(
                  child: Text('Fetching Data from Database',
                      style: TextStyle(
                          fontSize: 16,
                          letterSpacing: 0.3,
                          fontWeight: FontWeight.w600,
                          color: Colors.white54)),
                );
              }
            }))
          ],
        ),
      ),
    );
  }

  getTeamData(
      ControllerTeamStanding controllerTeamStanding, String dataTeamId) {
    for (var team in controllerTeamStanding.teamStanding) {
      if (team.teamId == dataTeamId) {
        return team;
      }
    }
  }
}
