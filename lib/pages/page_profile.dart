import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: Center(
          child: Container(
            width: 344,
            child: ListView(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(0, 66, 0, 58),
                  child: Center(
                    child: Image(
                      image: AssetImage('lib/shared/engshow_logo.png'),
                      width: 80,
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        backgroundColor: Color(0xFFC4C4C4),
                        radius: 72,
                        backgroundImage: AssetImage('lib/shared/foto.jpg'),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Flexible(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Tri Jaka Pamungkas',
                              style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.w700,
                                  letterSpacing: 0.3,
                                  color: Colors.white),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text(
                              'trijakapamungkas@gmail.com',
                              style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600,
                                  letterSpacing: 0.3,
                                  color: Color(0x50FFFFFF)),
                            ),
                          ],
                        ),
                      )
                    ]),
                SizedBox(
                  height: 28,
                ),
                Container(
                  margin: EdgeInsets.all(16),
                  padding: EdgeInsets.all(24),
                  decoration: BoxDecoration(
                      color: Color(0xFF2B2B2B),
                      border: Border.all(color: Color(0xFFFFB830)),
                      borderRadius: BorderRadius.circular(24)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.network(
                              'https://cdn-icons-png.flaticon.com/512/25/25231.png',
                              width: 24,
                              height: 24,
                            ),
                            SizedBox(width: 12),
                            Text(
                              'Profil Github',
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  letterSpacing: 0.3,
                                  color: Color(0x50FFFFFF)),
                            ),
                          ]),
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                        'Tri Jaka Pamungkas',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.3,
                            color: Color(0xFFFFFFFF)),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(16),
                  padding: EdgeInsets.all(24),
                  decoration: BoxDecoration(
                      color: Color(0xFF2B2B2B),
                      border: Border.all(color: Color(0xFFFFB830)),
                      borderRadius: BorderRadius.circular(24)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.network(
                              'http://assets.stickpng.com/images/5847f997cef1014c0b5e48c1.png',
                              width: 24,
                              height: 24,
                            ),
                            SizedBox(width: 12),
                            Text(
                              'Profil Gitlab',
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  letterSpacing: 0.3,
                                  color: Color(0x50FFFFFF)),
                            ),
                          ]),
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                        'Tri Jaka Pamungkas',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.3,
                            color: Color(0xFFFFFFFF)),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
