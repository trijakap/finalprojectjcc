import 'package:flutter/cupertino.dart';

class ModelTeamStanding {
  late final String teamId;
  late final String venue;
  late final int position;
  late final String teamCrest;
  late final String teamName;
  late final int playedGames;
  late final int goalDifference;
  late final int points;
  late final int won;
  late final int draw;
  late final int lost;
  late final int goalsFor;
  late final int goalsAgainst;

  ModelTeamStanding(
      {required this.teamId,
      required this.position,
      required this.teamCrest,
      required this.teamName,
      required this.playedGames,
      required this.goalDifference,
      required this.points,
      required this.draw,
      required this.goalsAgainst,
      required this.goalsFor,
      required this.lost,
      required this.won});
}
