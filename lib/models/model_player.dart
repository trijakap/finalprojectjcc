class ModelPlayer {
  late final String playerId;
  late final String playerName;
  late final String position;
  late final String nationality;
  late final String photoId;

  ModelPlayer({
    required this.playerId,
    required this.playerName,
    required this.position,
    required this.nationality,
    required this.photoId,
  });
}
