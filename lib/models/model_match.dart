class ModelMatch {
  late final int matchday;
  late final String homeTeamName;
  late final String awayTeamName;
  late final String venue;
  late final DateTime date;
  late final String homeTeamCrest;
  late final String awayTeamCrest;
  late final String gameStatus;
  late final homeTeamScore;
  late final awayTeamScore;

  ModelMatch(
      {required this.matchday,
      required this.homeTeamName,
      required this.awayTeamName,
      required this.venue,
      required this.date,
      required this.homeTeamCrest,
      required this.awayTeamCrest,
      required this.gameStatus,
      required this.homeTeamScore,
      required this.awayTeamScore});
}
